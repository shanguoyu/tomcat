/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.apache.coyote;

import org.apache.tomcat.util.net.SSLHostConfig;

import java.util.concurrent.Executor;

/**
 * Abstract the protocol implementation, including threading, etc.
 * <p>
 * This is the main interface to be implemented by a coyote protocol.
 * Adapter is the main interface to be implemented by a coyote servlet
 * container.
 *
 * @author Remy Maucherat
 * @author Costin Manolache
 * @see Adapter
 */
public interface ProtocolHandler {

    /**
     * 获取 Adapter
     */
    public Adapter getAdapter();

    /**
     * 设置 Adapter
     */
    public void setAdapter(Adapter adapter);

    /**
     * 获取 Executor
     */
    public Executor getExecutor();


    /**
     * init
     */
    public void init() throws Exception;

    /**
     * start
     */
    public void start() throws Exception;

    /**
     * 暂停
     */
    public void pause() throws Exception;

    /**
     * 重新开始
     */
    public void resume() throws Exception;


    /**
     * stop
     */
    public void stop() throws Exception;


    /**
     * destroy
     */
    public void destroy() throws Exception;

    /**
     * 优雅关闭服务端套接字
     */
    public void closeServerSocketGraceful();

    /**
     * 是否使用 APR 协议
     */
    public boolean isAprRequired();

    /**
     * 是否支持 Sendfile 系统调用来优化响应时间
     */
    public boolean isSendfileSupported();

    /**
     * 添加 SSL 配置
     */
    public void addSslHostConfig(SSLHostConfig sslHostConfig);

    /**
     * 查询所有 SSL 配置
     */
    public SSLHostConfig[] findSslHostConfigs();

    /**
     * 添加可升级协议
     */
    public void addUpgradeProtocol(UpgradeProtocol upgradeProtocol);

    /**
     * 查询所有可升级协议
     */
    public UpgradeProtocol[] findUpgradeProtocols();
}
