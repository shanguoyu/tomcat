/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.tomcat.util.collections;

/**
 * This is intended as a (mostly) GC-free alternative to
 * {@link java.util.concurrent.ConcurrentLinkedQueue} when the requirement is to
 * create a pool of re-usable objects with no requirement to shrink the pool.
 * The aim is to provide the bare minimum of required functionality as quickly
 * as possible with minimum garbage.
 *
 * @param <T> The type of object managed by this stack
 */
public class SynchronizedStack<T> {
    /**
     * 默认对象池大小
     */
    public static final int DEFAULT_SIZE = 128;
    /**
     * 默认 limit 值
     */
    private static final int DEFAULT_LIMIT = -1;
    /**
     * 当前容量
     */
    private int size;
    /**
     * 限制最大容量
     */
    private final int limit;

    /*
     * 指向堆栈中下一个可用对象的索引
     */
    private int index = -1;
    /**
     * 存储对象的栈，用数组实现
     */
    private Object[] stack;


    public SynchronizedStack() {
        this(DEFAULT_SIZE, DEFAULT_LIMIT);
    }

    public SynchronizedStack(int size, int limit) {
        // limit 大于 -1 且 size 大于 limit，那么当前 size 为 limit
        if (limit > -1 && size > limit) {
            this.size = limit;
        } else {
            // 否则直接取 size
            this.size = size;
        }
        this.limit = limit;
        stack = new Object[size];
    }

    /**
     * 向对象池中归还一个对象
     * @param obj
     * @return
     */
    public synchronized boolean push(T obj) {
        // 更新索引
        index++;
        // 对象池满了
        if (index == size) {
            // 条件1：没有最大容量限制
            // 条件2：有最大容量限制，并且当前容量还小于最大限制
            if (limit == -1 || size < limit) {
                // 扩容数组
                expand();
            } else {
                // 对象池容量满了又不扩容，那么对象的归还自然失败了
                index--;
                return false;
            }
        }
        // 放到对象池中
        stack[index] = obj;
        // 返回true表示归还成功
        return true;
    }

    /**
     * 从对象池获取对象
     * @return
     */
    @SuppressWarnings("unchecked")
    public synchronized T pop() {
        // 对象池没有对象，返回 null
        if (index == -1) {
            return null;
        }
        // 获得对象
        T result = (T) stack[index];
        // 更新index的同时把对应位置上设置为null
        stack[index--] = null;
        // 返回
        return result;
    }

    /**
     * 清空对象池中的对象
     */
    public synchronized void clear() {
        if (index > -1) {
            for (int i = 0; i < index + 1; i++) {
                stack[i] = null;
            }
        }
        index = -1;
    }

    private void expand() {
        int newSize = size * 2;
        if (limit != -1 && newSize > limit) {
            newSize = limit;
        }
        Object[] newStack = new Object[newSize];
        System.arraycopy(stack, 0, newStack, 0, size);
        // This is the only point where garbage is created by throwing away the
        // old array. Note it is only the array, not the contents, that becomes
        // garbage.
        stack = newStack;
        size = newSize;
    }
}
