/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina;

import org.apache.catalina.connector.Connector;
import org.apache.catalina.mapper.Mapper;

/**
 * A <strong>Service</strong> is a group of one or more
 * <strong>Connectors</strong> that share a single <strong>Container</strong>
 * to process their incoming requests.  This arrangement allows, for example,
 * a non-SSL and SSL connector to share the same population of web apps.
 * <p>
 * A given JVM can contain any number of Service instances; however, they are
 * completely independent of each other and share only the basic JVM facilities
 * and classes on the system class path.
 *
 * @author Craig R. McClanahan
 */
public interface Service extends Lifecycle {

    // ------------------------------------------------------------- Properties

    /**
     * 获取 Engine
     */
    public Engine getContainer();

    /**
     * 设置 Engine
     */
    public void setContainer(Engine engine);

    /**
     * 获取名称
     */
    public String getName();

    /**
     * 设置名称
     */
    public void setName(String name);

    /**
     * 获得 Server
     */
    public Server getServer();

    /**
     * 设置 Server
     */
    public void setServer(Server server);

    /**
     * 获取父类加载器
     */
    public ClassLoader getParentClassLoader();

    /**
     * 设置父类加载器
     */
    public void setParentClassLoader(ClassLoader parent);

    /**
     * JMX 注册时指定 Domain 字符串
     */
    public String getDomain();
    /**
     * 添加lian'jie'qi
     */
    public void addConnector(Connector connector);

    /**
     * 获得所有连接器
     */
    public Connector[] findConnectors();

    /**
     * 删除指定连接器
     */
    public void removeConnector(Connector connector);

    /**
     * 添加线程池
     */
    public void addExecutor(Executor ex);

    /**
     * 查询所有线程池
     */
    public Executor[] findExecutors();

    /**
     * 根据名称查询线程池
     */
    public Executor getExecutor(String name);

    /**
     * 移除指定线程池
     */
    public void removeExecutor(Executor ex);

    /**
     * 获得 Mapper
     */
    Mapper getMapper();
}
