/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina;

import org.apache.catalina.deploy.NamingResourcesImpl;
import org.apache.catalina.startup.Catalina;

import java.io.File;

/**
 * A <code>Server</code> element represents the entire Catalina
 * servlet container.  Its attributes represent the characteristics of
 * the servlet container as a whole.  A <code>Server</code> may contain
 * one or more <code>Services</code>, and the top level set of naming
 * resources.
 * <p>
 * Normally, an implementation of this interface will also implement
 * <code>Lifecycle</code>, such that when the <code>start()</code> and
 * <code>stop()</code> methods are called, all of the defined
 * <code>Services</code> are also started or stopped.
 * <p>
 * In between, the implementation must open a server socket on the port number
 * specified by the <code>port</code> property.  When a connection is accepted,
 * the first line is read and compared with the specified shutdown command.
 * If the command matches, shutdown of the server is initiated.
 * <p>
 * <strong>NOTE</strong> - The concrete implementation of this class should
 * register the (singleton) instance with the <code>ServerFactory</code>
 * class in its constructor(s).
 *
 * @author Craig R. McClanahan
 */
public interface Server extends Lifecycle {

    /**
     * 获取全局命名资源
     */
    public NamingResourcesImpl getGlobalNamingResources();


    /**
     * 设置全局命名资源
     *
     * @param globalNamingResources The new global naming resources
     */
    public void setGlobalNamingResources
    (NamingResourcesImpl globalNamingResources);


    /**
     * 获取上下文支持
     */
    public javax.naming.Context getGlobalNamingContext();


    /**
     * 获取监听 shutdown 命令的端口号
     */
    public int getPort();


    /**
     * 设置监听 shutdown 命令的端口号
     */
    public void setPort(int port);


    /**
     * 获取监听 shutdown 命令的地址
     */
    public String getAddress();


    /**
     * 设置监听 shutdown 命令的地址
     */
    public void setAddress(String address);


    /**
     * 监听 shutdown 命令的字符串支持
     */
    public String getShutdown();


    /**
     * 获取监听 shutdown 命令的字符串支持
     */
    public void setShutdown(String shutdown);

    /**
     * 父类加载器支持
     */
    public ClassLoader getParentClassLoader();

    /**
     * 父类加载器支持
     */
    public void setParentClassLoader(ClassLoader parent);


    /**
     * 获得 Catalina
     */
    public Catalina getCatalina();

    /**
     * 设置 Catalina
     */
    public void setCatalina(Catalina catalina);


    /**
     * 获取 Catalina Base
     */
    public File getCatalinaBase();

    /**
     * 设置 Catalina Base
     */
    public void setCatalinaBase(File catalinaBase);


    /**
     * 获取 Catalina Home
     */
    public File getCatalinaHome();

    /**
     * 设置 Catalina Home
     */
    public void setCatalinaHome(File catalinaHome);


    // --------------------------------------------------------- Public Methods


    /**
     * 添加一个 Service
     */
    public void addService(Service service);


    /**
     * 等待 Server 关闭
     */
    public void await();


    /**
     * 根据名称查找 Service
     */
    public Service findService(String name);


    /**
     * 查找所有 Service
     */
    public Service[] findServices();


    /**
     * 移除指定 Service
     */
    public void removeService(Service service);


    /**
     * 获取 联合 JNDI 上下文使用的 NamingToken
     */
    public Object getNamingToken();
}
