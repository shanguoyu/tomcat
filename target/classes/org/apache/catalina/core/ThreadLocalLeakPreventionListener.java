/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina.core;

import java.util.concurrent.Executor;

import org.apache.catalina.Container;
import org.apache.catalina.ContainerEvent;
import org.apache.catalina.ContainerListener;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.Server;
import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.ProtocolHandler;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;
import org.apache.tomcat.util.threads.ThreadPoolExecutor;

/**
 * A {@link LifecycleListener} that triggers the renewal of threads in Executor
 * pools when a {@link Context} is being stopped to avoid thread-local related
 * memory leaks.
 * <p>
 * Note : active threads will be renewed one by one when they come back to the
 * pool after executing their task, see
 * {@link org.apache.tomcat.util.threads.ThreadPoolExecutor}.afterExecute().
 * <p>
 * This listener must only be nested within {@link Server} elements.
 */
public class ThreadLocalLeakPreventionListener implements LifecycleListener,
        ContainerListener {

    private static final Log log =
        LogFactory.getLog(ThreadLocalLeakPreventionListener.class);
    /**
     * Server是否已经停止
     */
    private volatile boolean serverStopping = false;

    /**
     * The string manager for this package.
     */
    protected static final StringManager sm = StringManager.getManager(ThreadLocalLeakPreventionListener.class);

    /**
     * Listens for {@link LifecycleEvent} for the start of the {@link Server} to
     * initialize itself and then for after_stop events of each {@link Context}.
     */
    @Override
    public void lifecycleEvent(LifecycleEvent event) {
        try {
            // 获取事件源
            Lifecycle lifecycle = event.getLifecycle();
            // 响应 Server 组件的 AFTER_START_EVENT
            if (Lifecycle.AFTER_START_EVENT.equals(event.getType()) &&
                    lifecycle instanceof Server) {
                // 强转
                Server server = (Server) lifecycle;
                // 将监听器注册到 Server 所有子容器中
                registerListenersForServer(server);
            }
            // 响应 Server 组件的 before_stop_event 事件，将 serverStopping 标志位设置为true
            if (Lifecycle.BEFORE_STOP_EVENT.equals(event.getType()) &&
                    lifecycle instanceof Server) {
                serverStopping = true;
            }
            // 响应 Context 组件 AFTER_STOP_EVENT 事件，将线程容器刷新
            if (Lifecycle.AFTER_STOP_EVENT.equals(event.getType()) &&
                    lifecycle instanceof Context) {
                stopIdleThreads((Context) lifecycle);
            }
        } catch (Exception e) {
            String msg =
                sm.getString(
                    "threadLocalLeakPreventionListener.lifecycleEvent.error",
                    event);
            log.error(msg, e);
        }
    }

    @Override
    public void containerEvent(ContainerEvent event) {
        try {
            // 获得类型
            String type = event.getType();
            // 响应添加子容器事件
            if (Container.ADD_CHILD_EVENT.equals(type)) {
                processContainerAddChild(event.getContainer(),
                    (Container) event.getData());
            } else if (Container.REMOVE_CHILD_EVENT.equals(type)) {
                // 响应移除子容器事件
                processContainerRemoveChild(event.getContainer(),
                    (Container) event.getData());
            }
        } catch (Exception e) {
            String msg =
                sm.getString(
                    "threadLocalLeakPreventionListener.containerEvent.error",
                    event);
            log.error(msg, e);
        }

    }

    private void registerListenersForServer(Server server) {
        // 获得下面所有的 Service
        for (Service service : server.findServices()) {
            // 获取当前 Service 的容器
            Engine engine = service.getContainer();
            // 如果有Engine
            if (engine != null) {
                // 添加监听器
                engine.addContainerListener(this);
                //
                registerListenersForEngine(engine);
            }
        }

    }

    private void registerListenersForEngine(Engine engine) {
        // 获得引擎下面所有的Host
        for (Container hostContainer : engine.findChildren()) {
            Host host = (Host) hostContainer;
            // 添加监听器
            host.addContainerListener(this);
            // 遍历Host注册
            registerListenersForHost(host);
        }
    }

    private void registerListenersForHost(Host host) {
        // 遍历 Host 中所有 Context
        for (Container contextContainer : host.findChildren()) {
            Context context = (Context) contextContainer;
            // 添加 Context
            registerContextListener(context);
        }
    }

    private void registerContextListener(Context context) {
        context.addLifecycleListener(this);
    }

    protected void processContainerAddChild(Container parent, Container child) {
        if (log.isDebugEnabled()) {
            log.debug("Process addChild[parent=" + parent + ",child=" + child +
                "]");
        }
        if (child instanceof Context) {
            registerContextListener((Context) child);
        } else if (child instanceof Engine) {
            registerListenersForEngine((Engine) child);
        } else if (child instanceof Host) {
            registerListenersForHost((Host) child);
        }

    }

    protected void processContainerRemoveChild(Container parent,
        Container child) {

        if (log.isDebugEnabled()) {
            log.debug("Process removeChild[parent=" + parent + ",child=" +
                child + "]");
        }

        if (child instanceof Context) {
            Context context = (Context) child;
            context.removeLifecycleListener(this);
        } else if (child instanceof Host || child instanceof Engine) {
            child.removeContainerListener(this);
        }
    }

    /**
     * Updates each ThreadPoolExecutor with the current time, which is the time
     * when a context is being stopped.
     *
     * @param context
     *            the context being stopped, used to discover all the Connectors
     *            of its parent Service.
     */
    private void stopIdleThreads(Context context) {
        // 如果 server 已经关闭了则啥都不用做
        if (serverStopping) {
            return;
        }
        // Context类型不对直接返回
        if (!(context instanceof StandardContext) ||
            !((StandardContext) context).getRenewThreadsWhenStoppingContext()) {
            log.debug("Not renewing threads when the context is stopping. "
                + "It is not configured to do it.");
            return;
        }
        // 获取Context的Parent也就是Engine
        Engine engine = (Engine) context.getParent().getParent();
        // 获取Engine的上级也就是Service
        Service service = engine.getService();
        // 获取上下文的连接器对象
        Connector[] connectors = service.findConnectors();
        // 如果有
        if (connectors != null) {
            // 遍历
            for (Connector connector : connectors) {
                // 获得ProtocolHandler
                ProtocolHandler handler = connector.getProtocolHandler();
                // 获得线程池，如果有则获得
                Executor executor = null;
                if (handler != null) {
                    executor = handler.getExecutor();
                }
                // 调用处理器的 contextStopping 方法
                if (executor instanceof ThreadPoolExecutor) {
                    ThreadPoolExecutor threadPoolExecutor =
                        (ThreadPoolExecutor) executor;
                    threadPoolExecutor.contextStopping();
                } else if (executor instanceof StandardThreadExecutor) {
                    StandardThreadExecutor stdThreadExecutor =
                        (StandardThreadExecutor) executor;
                    stdThreadExecutor.contextStopping();
                }

            }
        }
    }
}
