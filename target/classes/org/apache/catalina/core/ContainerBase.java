/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina.core;

import org.apache.catalina.AccessLog;
import org.apache.catalina.Cluster;
import org.apache.catalina.Container;
import org.apache.catalina.ContainerEvent;
import org.apache.catalina.ContainerListener;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Globals;
import org.apache.catalina.Host;
import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.Loader;
import org.apache.catalina.Pipeline;
import org.apache.catalina.Realm;
import org.apache.catalina.Valve;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.util.ContextName;
import org.apache.catalina.util.LifecycleMBeanBase;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.ExceptionUtils;
import org.apache.tomcat.util.MultiThrowable;
import org.apache.tomcat.util.res.StringManager;

import javax.management.ObjectName;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * Abstract implementation of the <b>Container</b> interface, providing common
 * functionality required by nearly every implementation.  Classes extending
 * this base class must may implement a replacement for <code>invoke()</code>.
 * <p>
 * All subclasses of this abstract base class will include support for a
 * Pipeline object that defines the processing to be performed for each request
 * received by the <code>invoke()</code> method of this class, utilizing the
 * "Chain of Responsibility" design pattern.  A subclass should encapsulate its
 * own processing functionality as a <code>Valve</code>, and configure this
 * Valve into the pipeline by calling <code>setBasic()</code>.
 * <p>
 * This implementation fires property change events, per the JavaBeans design
 * pattern, for changes in singleton properties.  In addition, it fires the
 * following <code>ContainerEvent</code> events to listeners who register
 * themselves with <code>addContainerListener()</code>:
 * <table border=1>
 *   <caption>ContainerEvents fired by this implementation</caption>
 *   <tr>
 *     <th>Type</th>
 *     <th>Data</th>
 *     <th>Description</th>
 *   </tr>
 *   <tr>
 *     <td><code>addChild</code></td>
 *     <td><code>Container</code></td>
 *     <td>Child container added to this Container.</td>
 *   </tr>
 *   <tr>
 *     <td><code>{@link #getPipeline() pipeline}.addValve</code></td>
 *     <td><code>Valve</code></td>
 *     <td>Valve added to this Container.</td>
 *   </tr>
 *   <tr>
 *     <td><code>removeChild</code></td>
 *     <td><code>Container</code></td>
 *     <td>Child container removed from this Container.</td>
 *   </tr>
 *   <tr>
 *     <td><code>{@link #getPipeline() pipeline}.removeValve</code></td>
 *     <td><code>Valve</code></td>
 *     <td>Valve removed from this Container.</td>
 *   </tr>
 *   <tr>
 *     <td><code>start</code></td>
 *     <td><code>null</code></td>
 *     <td>Container was started.</td>
 *   </tr>
 *   <tr>
 *     <td><code>stop</code></td>
 *     <td><code>null</code></td>
 *     <td>Container was stopped.</td>
 *   </tr>
 * </table>
 * Subclasses that fire additional events should document them in the
 * class comments of the implementation class.
 *
 * @author Craig R. McClanahan
 */
public abstract class ContainerBase extends LifecycleMBeanBase implements Container {
    /**
     * 日志组件
     */
    private static final Log log = LogFactory.getLog(ContainerBase.class);

    /**
     * 添加子容器是在 XML 解析中执行的，所有的容器层级结构都在 server.xml 中定义，所以必定是通过解析 XML 创建层级结构对象
     * 需要使用该类提供对应的权限，当然这个东西可以忽略。因为通常不会使用 Java 安全管理器
     */
    protected class PrivilegedAddChild implements PrivilegedAction<Void> {

        private final Container child;

        PrivilegedAddChild(Container child) {
            this.child = child;
        }

        @Override
        public Void run() {
            addChildInternal(child);
            return null;
        }

    }


    // ----------------------------------------------------- Instance Variables


    /**
     * 存放子容器的HashMap
     */
    protected final HashMap<String, Container> children = new HashMap<>();


    /**
     * 容器周期性任务的延迟时间
     */
    protected int backgroundProcessorDelay = -1;


    /**
     * 存放容器监听器事件
     */
    protected final List<ContainerListener> listeners = new CopyOnWriteArrayList<>();

    /**
     * 与容器联合使用的日志组件
     */
    protected Log logger = null;


    /**
     * 日志对象名
     */
    protected String logName = null;


    /**
     * 与容器联合使用的集群对象和保护集群对象的读写锁
     */
    protected Cluster cluster = null;
    private final ReadWriteLock clusterLock = new ReentrantReadWriteLock();


    /**
     * 当前容器名
     */
    protected String name = null;


    /**
     * 当前容器的父容器
     */
    protected Container parent = null;


    /**
     * 当使用 Loader 组件时，使用的父类加载器
     */
    protected ClassLoader parentClassLoader = null;


    /**
     * 与容器联合使用的 Pipeline 对象
     */
    protected final Pipeline pipeline = new StandardPipeline(this);


    /**
     * 与容器联合使用的 Realm 对象
     */
    private volatile Realm realm = null;


    /**
     * 用于保护访问 Realm 对象的读写锁
     */
    private final ReadWriteLock realmLock = new ReentrantReadWriteLock();


    /**
     * 当前包中的类公用的字符串处理对象
     */
    protected static final StringManager sm = StringManager.getManager(ContainerBase.class);
    /**
     * 用于标识当子组件被添加时自动启动
     */
    protected boolean startChildren = true;

    /**
     * 用于支持属性修改
     */
    protected final PropertyChangeSupport support =
        new PropertyChangeSupport(this);

    /**
     * 后台执行周期性任务的线程
     */
    private Thread thread = null;
    /**
     * 用于标识后台周期性任务是否执行完成
     */
    private volatile boolean threadDone = false;
    /**
     * 记录容器访问日志和日志扫描完成标识
     */
    protected volatile AccessLog accessLog = null;
    private volatile boolean accessLogScanComplete = false;
    /**
     * 并行启动和停止子容器的线程数和线程池
     */
    private int startStopThreads = 1;
    protected ThreadPoolExecutor startStopExecutor;


    // ------------------------------------------------------------- Properties

    @Override
    public int getStartStopThreads() {
        return startStopThreads;
    }

    /**
     * Handles the special values.
     */
    private int getStartStopThreadsInternal() {
        // 获得 startStopThreads
        int result = getStartStopThreads();

        // 如果大于0则直接返回
        if (result > 0) {
            return result;
        }
        // 取 CPU 核心数加上 Result的数，保证最少为 1
        result = Runtime.getRuntime().availableProcessors() + result;
        // 如果还小于 1 就用 1
        if (result < 1) {
            result = 1;
        }
        return result;
    }

    @Override
    public void setStartStopThreads(int startStopThreads) {
        // 存进去
        this.startStopThreads = startStopThreads;
        ThreadPoolExecutor executor = startStopExecutor;
        if (executor != null) {
            int newThreads = getStartStopThreadsInternal();
            executor.setMaximumPoolSize(newThreads);
            executor.setCorePoolSize(newThreads);
        }
    }


    /**
     * Get the delay between the invocation of the backgroundProcess method on
     * this container and its children. Child containers will not be invoked
     * if their delay value is not negative (which would mean they are using
     * their own thread). Setting this to a positive value will cause
     * a thread to be spawn. After waiting the specified amount of time,
     * the thread will invoke the executePeriodic method on this container
     * and all its children.
     */
    @Override
    public int getBackgroundProcessorDelay() {
        return backgroundProcessorDelay;
    }


    /**
     * Set the delay between the invocation of the execute method on this
     * container and its children.
     *
     * @param delay The delay in seconds between the invocation of
     *              backgroundProcess methods
     */
    @Override
    public void setBackgroundProcessorDelay(int delay) {
        backgroundProcessorDelay = delay;
    }


    /**
     * Return the Logger for this Container.
     */
    @Override
    public Log getLogger() {
        if (logger != null) {
            return logger;
        }
        logger = LogFactory.getLog(getLogName());
        return logger;
    }


    /**
     * @return the abbreviated name of this container for logging messages
     */
    @Override
    public String getLogName() {

        if (logName != null) {
            return logName;
        }
        String loggerName = null;
        Container current = this;
        while (current != null) {
            String name = current.getName();
            if ((name == null) || (name.equals(""))) {
                name = "/";
            } else if (name.startsWith("##")) {
                name = "/" + name;
            }
            loggerName = "[" + name + "]"
                + ((loggerName != null) ? ("." + loggerName) : "");
            current = current.getParent();
        }
        logName = ContainerBase.class.getName() + "." + loggerName;
        return logName;

    }


    /**
     * Return the Cluster with which this Container is associated.  If there is
     * no associated Cluster, return the Cluster associated with our parent
     * Container (if any); otherwise return <code>null</code>.
     */
    @Override
    public Cluster getCluster() {
        // 获取读锁
        Lock readLock = clusterLock.readLock();
        // 上读锁
        readLock.lock();
        try {
            // 获取当前容器有没有 Cluster 如果有则直接返回
            if (cluster != null) {
                return cluster;
            }
            // 走到这里说明当前容器没有，则调用父容器查询
            if (parent != null) {
                return parent.getCluster();
            }

            return null;
        } finally {
            // 解开读锁
            readLock.unlock();
        }
    }


    /*
     * Provide access to just the cluster component attached to this container.
     */
    protected Cluster getClusterInternal() {
        // 获取读锁
        Lock readLock = clusterLock.readLock();
        // 上读锁
        readLock.lock();
        try {
            // 返回当前容器的 Cluster
            return cluster;
        } finally {
            // 解读锁
            readLock.unlock();
        }
    }


    /**
     * Set the Cluster with which this Container is associated.
     *
     * @param cluster The newly associated Cluster
     */
    @Override
    public void setCluster(Cluster cluster) {
        Cluster oldCluster = null;
        // 获取写锁
        Lock writeLock = clusterLock.writeLock();
        // 上写锁
        writeLock.lock();
        try {
            // 获得当前容器的 Cluster
            oldCluster = this.cluster;
            // 如果当前容器的 Cluster 等于 参数 则直接 return
            if (oldCluster == cluster) {
                return;
            }
            // 走到这说明不一样 则把参数设置给属性
            this.cluster = cluster;

            // 如果替换后的 Cluster 对象实现了生命周期函数，则让其进入停止流程
            if (getState().isAvailable() && (oldCluster != null) &&
                (oldCluster instanceof Lifecycle)) {
                try {
                    ((Lifecycle) oldCluster).stop();
                } catch (LifecycleException e) {
                    log.error(sm.getString("containerBase.cluster.stop"), e);
                }
            }

            // 如果新的 Cluster 实现了生命周期函数，那么让其进入开始流程
            if (cluster != null) {
                cluster.setContainer(this);
            }

            if (getState().isAvailable() && (cluster != null) &&
                (cluster instanceof Lifecycle)) {
                try {
                    ((Lifecycle) cluster).start();
                } catch (LifecycleException e) {
                    log.error(sm.getString("containerBase.cluster.start"), e);
                }
            }
        } finally {
            // 释放写锁
            writeLock.unlock();
        }

        // 通知属性变化监听器当前容器的 cluster 属性发生了改变
        support.firePropertyChange("cluster", oldCluster, cluster);
    }


    /**
     * Return a name string (suitable for use by humans) that describes this
     * Container.  Within the set of child containers belonging to a particular
     * parent, Container names must be unique.
     */
    @Override
    public String getName() {
        return name;
    }


    /**
     * Set a name string (suitable for use by humans) that describes this
     * Container.  Within the set of child containers belonging to a particular
     * parent, Container names must be unique.
     *
     * @param name New name of this container
     * @throws IllegalStateException if this Container has already been
     *                               added to the children of a parent Container (after which the name
     *                               may not be changed)
     */
    @Override
    public void setName(String name) {
        // 参数校验
        if (name == null) {
            throw new IllegalArgumentException(sm.getString("containerBase.nullName"));
        }
        // 获取原本的容器名
        String oldName = this.name;
        // 赋值新的容器名
        this.name = name;
        // 通知属性变化监听器当前容器的name属性已经改变
        support.firePropertyChange("name", oldName, this.name);
    }


    /**
     * Return if children of this container will be started automatically when
     * they are added to this container.
     *
     * @return <code>true</code> if the children will be started
     */
    public boolean getStartChildren() {
        return startChildren;
    }


    /**
     * Set if children of this container will be started automatically when
     * they are added to this container.
     *
     * @param startChildren New value of the startChildren flag
     */
    public void setStartChildren(boolean startChildren) {

        boolean oldStartChildren = this.startChildren;
        this.startChildren = startChildren;
        support.firePropertyChange("startChildren", oldStartChildren, this.startChildren);
    }


    /**
     * Return the Container for which this Container is a child, if there is
     * one.  If there is no defined parent, return <code>null</code>.
     */
    @Override
    public Container getParent() {
        return parent;
    }


    /**
     * Set the parent Container to which this Container is being added as a
     * child.  This Container may refuse to become attached to the specified
     * Container by throwing an exception.
     *
     * @param container Container to which this Container is being added
     *                  as a child
     * @throws IllegalArgumentException if this Container refuses to become
     *                                  attached to the specified Container
     */
    @Override
    public void setParent(Container container) {

        Container oldParent = this.parent;
        this.parent = container;
        // 通知属性变化监听器当前容器的 parent 属性已经发生改变
        support.firePropertyChange("parent", oldParent, this.parent);

    }


    /**
     * Return the parent class loader (if any) for this web application.
     * This call is meaningful only <strong>after</strong> a Loader has
     * been configured.
     */
    @Override
    public ClassLoader getParentClassLoader() {
        // 获取父类加载器。如果已经设置了 parentClassLoader，那么直接返回，否则尝试从父容器获取，如果父容器也没有，那么获取系统类加载器

        if (parentClassLoader != null) {
            return parentClassLoader;
        }
        if (parent != null) {
            return parent.getParentClassLoader();
        }
        return ClassLoader.getSystemClassLoader();
    }


    /**
     * Set the parent class loader (if any) for this web application.
     * This call is meaningful only <strong>before</strong> a Loader has
     * been configured, and the specified value (if non-null) should be
     * passed as an argument to the class loader constructor.
     *
     * @param parent The new parent class loader
     */
    @Override
    public void setParentClassLoader(ClassLoader parent) {
        ClassLoader oldParentClassLoader = this.parentClassLoader;
        this.parentClassLoader = parent;
        // 通知属性变化监听器当前容器的 parentClassLoader 属性已经改变
        support.firePropertyChange("parentClassLoader", oldParentClassLoader,
            this.parentClassLoader);

    }


    /**
     * Return the Pipeline object that manages the Valves associated with
     * this Container.
     */
    @Override
    public Pipeline getPipeline() {
        return this.pipeline;
    }


    /**
     * Return the Realm with which this Container is associated.  If there is
     * no associated Realm, return the Realm associated with our parent
     * Container (if any); otherwise return <code>null</code>.
     */
    @Override
    public Realm getRealm() {
        // 不在举例
        Lock l = realmLock.readLock();
        l.lock();
        try {
            if (realm != null) {
                return realm;
            }
            if (parent != null) {
                return parent.getRealm();
            }
            return null;
        } finally {
            l.unlock();
        }
    }


    protected Realm getRealmInternal() {
        Lock l = realmLock.readLock();
        l.lock();
        try {
            return realm;
        } finally {
            l.unlock();
        }
    }

    /**
     * Set the Realm with which this Container is associated.
     *
     * @param realm The newly associated Realm
     */
    @Override
    public void setRealm(Realm realm) {

        Lock l = realmLock.writeLock();
        l.lock();
        try {
            // Change components if necessary
            Realm oldRealm = this.realm;
            if (oldRealm == realm) {
                return;
            }
            this.realm = realm;

            // Stop the old component if necessary
            if (getState().isAvailable() && (oldRealm != null) &&
                (oldRealm instanceof Lifecycle)) {
                try {
                    ((Lifecycle) oldRealm).stop();
                } catch (LifecycleException e) {
                    log.error(sm.getString("containerBase.realm.stop"), e);
                }
            }

            // Start the new component if necessary
            if (realm != null) {
                realm.setContainer(this);
            }
            if (getState().isAvailable() && (realm != null) &&
                (realm instanceof Lifecycle)) {
                try {
                    ((Lifecycle) realm).start();
                } catch (LifecycleException e) {
                    log.error(sm.getString("containerBase.realm.start"), e);
                }
            }

            // Report this property change to interested listeners
            support.firePropertyChange("realm", oldRealm, this.realm);
        } finally {
            l.unlock();
        }

    }


    // ------------------------------------------------------ Container Methods


    /**
     * Add a new child Container to those associated with this Container,
     * if supported.  Prior to adding this Container to the set of children,
     * the child's <code>setParent()</code> method must be called, with this
     * Container as an argument.  This method may thrown an
     * <code>IllegalArgumentException</code> if this Container chooses not
     * to be attached to the specified Container, in which case it is not added
     *
     * @param child New child Container to be added
     * @throws IllegalArgumentException if this exception is thrown by
     *                                  the <code>setParent()</code> method of the child Container
     * @throws IllegalArgumentException if the new child does not have
     *                                  a name unique from that of existing children of this Container
     * @throws IllegalStateException    if this Container does not support
     *                                  child Containers
     */
    @Override
    public void addChild(Container child) {
        if (Globals.IS_SECURITY_ENABLED) {
            PrivilegedAction<Void> dp =
                new PrivilegedAddChild(child);
            AccessController.doPrivileged(dp);
        } else {
            // 没有使用 Java 安全管理器时，通过以下方式添加
            addChildInternal(child);
        }
    }

    private void addChildInternal(Container child) {
        // debug 日志
        if (log.isDebugEnabled()) {
            log.debug("Add child " + child + " " + this);
        }
        // HashMap 上锁
        synchronized (children) {
            // 如果子容器已经存在了，则抛出异常
            if (children.get(child.getName()) != null) {
                throw new IllegalArgumentException(
                    sm.getString("containerBase.child.notUnique", child.getName()));
            }
            // 设置子的 parent
            child.setParent(this);
            // 添加到 children 中
            children.put(child.getName(), child);
        }
        try {
            // 如果当前容器处于 starting_prep 阶段并且设置了 startChildren 标志，那么启动子容器
            if ((getState().isAvailable() ||
                LifecycleState.STARTING_PREP.equals(getState())) &&
                startChildren) {
                // 启动子容器
                child.start();
            }
        } catch (LifecycleException e) {
            log.error("ContainerBase.addChild: start: ", e);
            throw new IllegalStateException(sm.getString("containerBase.child.start"), e);
        } finally {
            // 通知容器事件监听器，当前容器添加了子容器
            fireContainerEvent(ADD_CHILD_EVENT, child);
        }
    }


    /**
     * Add a container event listener to this component.
     *
     * @param listener The listener to add
     */
    @Override
    public void addContainerListener(ContainerListener listener) {
        listeners.add(listener);
    }


    /**
     * Add a property change listener to this component.
     *
     * @param listener The listener to add
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }


    /**
     * Return the child Container, associated with this Container, with
     * the specified name (if any); otherwise, return <code>null</code>
     *
     * @param name Name of the child Container to be retrieved
     */
    @Override
    public Container findChild(String name) {
        if (name == null) {
            return null;
        }
        synchronized (children) {
            return children.get(name);
        }
    }


    /**
     * Return the set of children Containers associated with this Container.
     * If this Container has no children, a zero-length array is returned.
     */
    @Override
    public Container[] findChildren() {
        // 上锁
        synchronized (children) {
            // 创建数组
            Container results[] = new Container[children.size()];
            // 获取所有子容器，注意这里是快照值
            return children.values().toArray(results);
        }
    }


    /**
     * Return the set of container listeners associated with this Container.
     * If this Container has no registered container listeners, a zero-length
     * array is returned.
     */
    @Override
    public ContainerListener[] findContainerListeners() {
        ContainerListener[] results =
            new ContainerListener[0];
        return listeners.toArray(results);
    }


    /**
     * Remove an existing child Container from association with this parent
     * Container.
     *
     * @param child Existing child Container to be removed
     */
    @Override
    public void removeChild(Container child) {

        if (child == null) {
            return;
        }

        try {
            // 如果子容器状态正常，则让其进入停止流程
            if (child.getState().isAvailable()) {
                // 停止子容器
                child.stop();
            }
        } catch (LifecycleException e) {
            log.error(sm.getString("containerBase.child.stop"), e);
        }

        try {
            // 子容器还没有进入销毁阶段，那么让其进入
            if (!LifecycleState.DESTROYING.equals(child.getState())) {
                child.destroy();
            }
        } catch (LifecycleException e) {
            log.error(sm.getString("containerBase.child.destroy"), e);
        }
        // 从 Map 中移除 子容器
        synchronized (children) {
            if (children.get(child.getName()) == null) {
                return;
            }
            children.remove(child.getName());
        }
        // 通知容器事件监听器，当前容器移除了子容器
        fireContainerEvent(REMOVE_CHILD_EVENT, child);
    }


    /**
     * Remove a container event listener from this component.
     *
     * @param listener The listener to remove
     */
    @Override
    public void removeContainerListener(ContainerListener listener) {
        listeners.remove(listener);
    }


    /**
     * Remove a property change listener from this component.
     *
     * @param listener The listener to remove
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {

        support.removePropertyChangeListener(listener);

    }


    @Override
    protected void initInternal() throws LifecycleException {
        // 初始化 startStopExecutor 线程池
        BlockingQueue<Runnable> startStopQueue = new LinkedBlockingQueue<>();
        startStopExecutor = new ThreadPoolExecutor(
            getStartStopThreadsInternal(),
            getStartStopThreadsInternal(), 10, TimeUnit.SECONDS,
            startStopQueue,
            new StartStopThreadFactory(getName() + "-startStop-"));
        // 核心线程超时也可以退出
        startStopExecutor.allowCoreThreadTimeOut(true);
        // 调用父类的initInternal，主要是与JMX有关
        super.initInternal();
    }


    /**
     * Start this component and implement the requirements
     * of {@link org.apache.catalina.util.LifecycleBase#startInternal()}.
     *
     * @throws LifecycleException if this component detects a fatal error
     *                            that prevents this component from being used
     */
    @Override
    protected synchronized void startInternal() throws LifecycleException {
        // 获取日志对象组件
        logger = null;
        getLogger();
        // 启动当前容器的集群组件
        Cluster cluster = getClusterInternal();
        if (cluster instanceof Lifecycle) {
            ((Lifecycle) cluster).start();
        }
        // 启动当前容器的 Realm 组件
        Realm realm = getRealmInternal();
        if (realm instanceof Lifecycle) {
            ((Lifecycle) realm).start();
        }
        // 将所有子容器放在线程池中启动
        Container children[] = findChildren();
        List<Future<Void>> results = new ArrayList<>();
        for (Container child : children) {
            results.add(startStopExecutor.submit(new StartChild(child)));
        }

        MultiThrowable multiThrowable = null;
        // 等待线程池结果，如果出现异常，那么通过 MultiThrowable 汇总异常信息
        for (Future<Void> result : results) {
            try {
                result.get();
            } catch (Throwable e) {
                log.error(sm.getString("containerBase.threadedStartFailed"), e);
                if (multiThrowable == null) {
                    multiThrowable = new MultiThrowable();
                }
                multiThrowable.add(e);
            }

        }
        // 如果有异常则抛出
        if (multiThrowable != null) {
            throw new LifecycleException(sm.getString("containerBase.threadedStartFailed"),
                multiThrowable.getThrowable());
        }

        // 启动 Pipeline 组件
        if (pipeline instanceof Lifecycle) {
            ((Lifecycle) pipeline).start();
        }
        // 将状态修改为 STARTING
        setState(LifecycleState.STARTING);

        // 启动执行周期性任务的线程
        threadStart();
    }


    /**
     * Stop this component and implement the requirements
     * of {@link org.apache.catalina.util.LifecycleBase#stopInternal()}.
     *
     * @throws LifecycleException if this component detects a fatal error
     *                            that prevents this component from being used
     */
    @Override
    protected synchronized void stopInternal() throws LifecycleException {

        // 停止执行周期性任务的线程
        threadStop();
        // 设置状态为 STOPPING
        setState(LifecycleState.STOPPING);

        // 停止 Pipeline 组件
        if (pipeline instanceof Lifecycle &&
            ((Lifecycle) pipeline).getState().isAvailable()) {
            ((Lifecycle) pipeline).stop();
        }

        // 将所有子容器放入到线程池中停止
        Container children[] = findChildren();
        List<Future<Void>> results = new ArrayList<>();
        for (Container child : children) {
            results.add(startStopExecutor.submit(new StopChild(child)));
        }
        // 等待停止结果并收集异常信息
        boolean fail = false;
        for (Future<Void> result : results) {
            try {
                result.get();
            } catch (Exception e) {
                log.error(sm.getString("containerBase.threadedStopFailed"), e);
                fail = true;
            }
        }
        // 如果有异常则跑出去
        if (fail) {
            throw new LifecycleException(
                sm.getString("containerBase.threadedStopFailed"));
        }

        // 停止容器自己的 Cluster 和 Realm
        Realm realm = getRealmInternal();
        if (realm instanceof Lifecycle) {
            ((Lifecycle) realm).stop();
        }
        Cluster cluster = getClusterInternal();
        if (cluster instanceof Lifecycle) {
            ((Lifecycle) cluster).stop();
        }
    }

    @Override
    protected void destroyInternal() throws LifecycleException {
        // 销毁容器自己的 Realm 和 Cluster 组件
        Realm realm = getRealmInternal();
        if (realm instanceof Lifecycle) {
            ((Lifecycle) realm).destroy();
        }
        Cluster cluster = getClusterInternal();
        if (cluster instanceof Lifecycle) {
            ((Lifecycle) cluster).destroy();
        }

        // 销毁 Pipeline 组件
        if (pipeline instanceof Lifecycle) {
            ((Lifecycle) pipeline).destroy();
        }

        // 移除所有子容器
        for (Container child : findChildren()) {
            removeChild(child);
        }

        // 如果父容器存在，从父容器中移除当前容器
        if (parent != null) {
            parent.removeChild(this);
        }

        // 停止线程池
        if (startStopExecutor != null) {
            startStopExecutor.shutdownNow();
        }
        // JMX有关省略
        super.destroyInternal();
    }


    /**
     * Check this container for an access log and if none is found, look to the
     * parent. If there is no parent and still none is found, use the NoOp
     * access log.
     */
    @Override
    public void logAccess(Request request, Response response, long time,
                          boolean useDefault) {

        boolean logged = false;
        // 通过 AccessLog 组件记录
        if (getAccessLog() != null) {
            getAccessLog().log(request, response, time);
            logged = true;
        }
        // 如果父容器存在，调用父容器 logAccess 根据 useDefault 和 logged 决定是否记录日志
        if (getParent() != null) {
            // No need to use default logger once request/response has been logged
            // once
            getParent().logAccess(request, response, time, (useDefault && !logged));
        }
    }

    @Override
    public AccessLog getAccessLog() {
        // 如果为 true，即扫描完成，那么直接返回 accessLog
        if (accessLogScanComplete) {
            return accessLog;
        }
        // 否则遍历当前 Pipeline 组件中的 Valve，找到定义的 AccessLog 组件，然后将其添加到 AccessLogAdapter 中
        AccessLogAdapter adapter = null;
        Valve valves[] = getPipeline().getValves();
        for (Valve valve : valves) {
            if (valve instanceof AccessLog) {
                if (adapter == null) {
                    adapter = new AccessLogAdapter((AccessLog) valve);
                } else {
                    adapter.add((AccessLog) valve);
                }
            }
        }
        // 设置属性
        if (adapter != null) {
            accessLog = adapter;
        }
        // 设置扫描完成
        accessLogScanComplete = true;
        // 返回
        return accessLog;
    }

    // ------------------------------------------------------- Pipeline Methods


    /**
     * Convenience method, intended for use by the digester to simplify the
     * process of adding Valves to containers. See
     * {@link Pipeline#addValve(Valve)} for full details. Components other than
     * the digester should use {@link #getPipeline()}.{@link #addValve(Valve)} in case a
     * future implementation provides an alternative method for the digester to
     * use.
     *
     * @param valve Valve to be added
     * @throws IllegalArgumentException if this Container refused to
     *                                  accept the specified Valve
     * @throws IllegalArgumentException if the specified Valve refuses to be
     *                                  associated with this Container
     * @throws IllegalStateException    if the specified Valve is already
     *                                  associated with a different Container
     */
    public synchronized void addValve(Valve valve) {
        pipeline.addValve(valve);
    }


    /**
     * Execute a periodic task, such as reloading, etc. This method will be
     * invoked inside the classloading context of this container. Unexpected
     * throwables will be caught and logged.
     */
    @Override
    public void backgroundProcess() {
        // 当前容器状态不可用，那么直接返回
        if (!getState().isAvailable()) {
            return;
        }
        // 获得当前容器的 Cluster
        Cluster cluster = getClusterInternal();
        // 如果有
        if (cluster != null) {
            try {
                // 调用 Cluster 的 backgroundProcess
                cluster.backgroundProcess();
            } catch (Exception e) {
                log.warn(sm.getString("containerBase.backgroundProcess.cluster",
                    cluster), e);
            }
        }
        // 同样的逻辑 Realm
        Realm realm = getRealmInternal();
        if (realm != null) {
            try {
                realm.backgroundProcess();
            } catch (Exception e) {
                log.warn(sm.getString("containerBase.backgroundProcess.realm", realm), e);
            }
        }
        // 获得 Pipeline 的头
        Valve current = pipeline.getFirst();
        // 遍历 Pipeline
        while (current != null) {
            try {
                // 调用当前 Valve 的 backgroundProcess
                current.backgroundProcess();
            } catch (Exception e) {
                log.warn(sm.getString("containerBase.backgroundProcess.valve", current), e);
            }
            // 获得下一个
            current = current.getNext();
        }
        // 通知容器事件监听器，当前执行了周期性任务事件
        fireLifecycleEvent(Lifecycle.PERIODIC_EVENT, null);
    }


    @Override
    public File getCatalinaBase() {
        // 如果没有 Parent 则返回NULL
        if (parent == null) {
            return null;
        }
        // 有则调用父 Parent
        return parent.getCatalinaBase();
    }


    @Override
    public File getCatalinaHome() {

        if (parent == null) {
            return null;
        }

        return parent.getCatalinaHome();
    }


    // ------------------------------------------------------ Protected Methods

    /**
     * Notify all container event listeners that a particular event has
     * occurred for this Container.  The default implementation performs
     * this notification synchronously using the calling thread.
     *
     * @param type Event type
     * @param data Event data
     */
    @Override
    public void fireContainerEvent(String type, Object data) {
        // 如果没有监听器则直接返回
        if (listeners.size() < 1) {
            return;
        }
        // 创建事件
        ContainerEvent event = new ContainerEvent(this, type, data);
        // 遍历所有监听器
        for (ContainerListener listener : listeners) {
            // 唤醒所有监听器
            listener.containerEvent(event);
        }
    }


    // -------------------- JMX and Registration  --------------------

    @Override
    protected String getDomainInternal() {

        Container p = this.getParent();
        if (p == null) {
            return null;
        } else {
            return p.getDomain();
        }
    }


    @Override
    public String getMBeanKeyProperties() {
        Container c = this;
        StringBuilder keyProperties = new StringBuilder();
        int containerCount = 0;

        // Work up container hierarchy, add a component to the name for
        // each container
        while (!(c instanceof Engine)) {
            if (c instanceof Wrapper) {
                keyProperties.insert(0, ",servlet=");
                keyProperties.insert(9, c.getName());
            } else if (c instanceof Context) {
                keyProperties.insert(0, ",context=");
                ContextName cn = new ContextName(c.getName(), false);
                keyProperties.insert(9, cn.getDisplayName());
            } else if (c instanceof Host) {
                keyProperties.insert(0, ",host=");
                keyProperties.insert(6, c.getName());
            } else if (c == null) {
                // May happen in unit testing and/or some embedding scenarios
                keyProperties.append(",container");
                keyProperties.append(containerCount++);
                keyProperties.append("=null");
                break;
            } else {
                // Should never happen...
                keyProperties.append(",container");
                keyProperties.append(containerCount++);
                keyProperties.append('=');
                keyProperties.append(c.getName());
            }
            c = c.getParent();
        }
        return keyProperties.toString();
    }

    public ObjectName[] getChildren() {
        List<ObjectName> names = new ArrayList<>(children.size());
        for (Container next : children.values()) {
            if (next instanceof ContainerBase) {
                names.add(((ContainerBase) next).getObjectName());
            }
        }
        return names.toArray(new ObjectName[0]);
    }


    // -------------------- Background Thread --------------------

    /**
     * Start the background thread that will periodically check for
     * session timeouts.
     */
    protected void threadStart() {
        // 如果已经启动过了则直接 return
        if (thread != null) {
            return;
        }
        // 如果 延迟时间小于等于0则 return
        if (backgroundProcessorDelay <= 0) {
            return;
        }
        // 否则创建守护线程开始执行 ContainerBackgroundProcessor 对象
        threadDone = false;
        String threadName = "ContainerBackgroundProcessor[" + toString() + "]";
        thread = new Thread(new ContainerBackgroundProcessor(), threadName);
        thread.setDaemon(true);
        thread.start();

    }


    /**
     * Stop the background thread that is periodically checking for
     * session timeouts.
     */
    protected void threadStop() {
        // 如果没有对应的线程对象则 return
        if (thread == null) {
            return;
        }
        // 后台周期性任务已经完成
        threadDone = true;
        // 唤醒它
        thread.interrupt();
        try {
            // 等待线程执行结束
            thread.join();
        } catch (InterruptedException e) {
            // Ignore
        }
        // 设置为 null
        thread = null;

    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Container parent = getParent();
        if (parent != null) {
            sb.append(parent.toString());
            sb.append('.');
        }
        sb.append(this.getClass().getSimpleName());
        sb.append('[');
        sb.append(getName());
        sb.append(']');
        return sb.toString();
    }


    // -------------------------------------- ContainerExecuteDelay Inner Class

    /**
     * 后台周期性任务的线程执行体
     */
    protected class ContainerBackgroundProcessor implements Runnable {

        @Override
        public void run() {
            Throwable t = null;
            String unexpectedDeathMessage = sm.getString(
                "containerBase.backgroundProcess.unexpectedThreadDeath",
                Thread.currentThread().getName());
            try {
                // 如果没有完成
                while (!threadDone) {
                    try {
                        // 按照指定延期时间休息一会
                        Thread.sleep(backgroundProcessorDelay * 1000L);
                    } catch (InterruptedException e) {
                        // Ignore
                    }
                    // 如果还没有完成
                    if (!threadDone) {
                        // 执行周期性任务
                        processChildren(ContainerBase.this);
                    }
                }
            } catch (RuntimeException | Error e) {
                t = e;
                throw e;
            } finally {
                // 如果还没有完成则打印异常日志
                if (!threadDone) {
                    log.error(unexpectedDeathMessage, t);
                }
            }
        }

        protected void processChildren(Container container) {
            ClassLoader originalClassLoader = null;

            try {
                // 如果当前容器是 Context
                if (container instanceof Context) {
                    // 获得 Loader
                    Loader loader = ((Context) container).getLoader();
                    // 如果是 NULL 直接返回
                    if (loader == null) {
                        return;
                    }
                    // 确保当前 Loader 组件下的 Context 和 Wrapper 都被执行完毕
                    originalClassLoader = ((Context) container).bind(false, null);
                }
                // 执行当前容器的 backgroundProcess 方法
                container.backgroundProcess();
                // 执行所有定义了周期性任务的子容器的周期性任务
                Container[] children = container.findChildren();
                for (Container child : children) {
                    // 子容器没有开启自己的后台执行线程，那么由父容器执行子容器的周期性操作
                    if (child.getBackgroundProcessorDelay() <= 0) {
                        processChildren(child);
                    }
                }
            } catch (Throwable t) {
                ExceptionUtils.handleThrowable(t);
                log.error(sm.getString("containerBase.backgroundProcess.error"), t);
            } finally {
                // 如果当前容器是 Context 容器，那么解绑
                if (container instanceof Context) {
                    ((Context) container).unbind(false, originalClassLoader);
                }
            }
        }
    }


    // ---------------------------- Inner classes used with start/stop Executor

    /**
     * 启动子容器的知兴替
     */
    private static class StartChild implements Callable<Void> {
        /**子容器*/
        private Container child;

        public StartChild(Container child) {
            this.child = child;
        }

        @Override
        public Void call() throws LifecycleException {
            // 启动子容器
            child.start();
            return null;
        }
    }

    /**
     * 停止子容器执行体
     */
    private static class StopChild implements Callable<Void> {
        /**子容器*/
        private Container child;

        public StopChild(Container child) {
            this.child = child;
        }

        @Override
        public Void call() throws LifecycleException {
            // 当前状态下组件是否可用
            if (child.getState().isAvailable()) {
                // 可用就停止
                child.stop();
            }
            return null;
        }
    }

    /**
     * 用于创建线程池的线程工厂
     */
    private static class StartStopThreadFactory implements ThreadFactory {
        private final ThreadGroup group;
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        public StartStopThreadFactory(String namePrefix) {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
            this.namePrefix = namePrefix;
        }

        @Override
        public Thread newThread(Runnable r) {
            // 创建线程
            Thread thread = new Thread(group, r, namePrefix + threadNumber.getAndIncrement());
            // 设置为守护线程
            thread.setDaemon(true);
            // 返回
            return thread;
        }
    }
}
