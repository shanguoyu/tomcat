/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina;

/**
 * 生命周期状态
 */
public enum LifecycleState {
    /**
     * 组件新建状态
     */
    NEW(false, null),
    /**
     * 组件初始化状态
     */
    INITIALIZING(false, Lifecycle.BEFORE_INIT_EVENT),
    /**
     * 组件初始化后状态
     */
    INITIALIZED(false, Lifecycle.AFTER_INIT_EVENT),
    /**
     * 组件开始前状态
     */
    STARTING_PREP(false, Lifecycle.BEFORE_START_EVENT),
    /**
     * 组件开始中状态
     */
    STARTING(true, Lifecycle.START_EVENT),
    /**
     * 组件开始后状态
     */
    STARTED(true, Lifecycle.AFTER_START_EVENT),
    /**
     * 组件停止前状态
     */
    STOPPING_PREP(true, Lifecycle.BEFORE_STOP_EVENT),
    /**
     * 组件停止中状态
     */
    STOPPING(false, Lifecycle.STOP_EVENT),
    /**
     * 组件停止后状态
     */
    STOPPED(false, Lifecycle.AFTER_STOP_EVENT),
    /**
     * 组件销毁前状态
     */
    DESTROYING(false, Lifecycle.BEFORE_DESTROY_EVENT),
    /**
     * 组件销毁后状态
     */
    DESTROYED(false, Lifecycle.AFTER_DESTROY_EVENT),
    /**
     * 组件失败后状态
     */
    FAILED(false, null);
    /**
     * 当前状态下，组件是否可用
     */
    private final boolean available;
    /**
     * 当前状态下，组件对应的生命周期事件字符串
     */
    private final String lifecycleEvent;

    private LifecycleState(boolean available, String lifecycleEvent) {
        this.available = available;
        this.lifecycleEvent = lifecycleEvent;
    }

    /**
     * May the public methods other than property getters/setters and lifecycle
     * methods be called for a component in this state? It returns
     * <code>true</code> for any component in any of the following states:
     * <ul>
     * <li>{@link #STARTING}</li>
     * <li>{@link #STARTED}</li>
     * <li>{@link #STOPPING_PREP}</li>
     * </ul>
     *
     * @return <code>true</code> if the component is available for use,
     * otherwise <code>false</code>
     */
    public boolean isAvailable() {
        return available;
    }

    public String getLifecycleEvent() {
        return lifecycleEvent;
    }
}
