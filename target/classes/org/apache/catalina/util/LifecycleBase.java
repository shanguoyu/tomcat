/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina.util;

import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.ExceptionUtils;
import org.apache.tomcat.util.res.StringManager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Base implementation of the {@link Lifecycle} interface that implements the
 * state transition rules for {@link Lifecycle#start()} and
 * {@link Lifecycle#stop()}
 * Lifecycle 接口的基本实现
 * 在Lifecycle中声明了 生命周期的方法 init start stop destory方法
 * 声明了监听器和相关的事件
 * 在 LifecycleBase中实现了基本的串联。
 */
public abstract class LifecycleBase implements Lifecycle {
    /**
     * 日志打印对象
     */
    private static final Log log = LogFactory.getLog(LifecycleBase.class);
    /**
     * 国际化字符串对象
     */
    private static final StringManager sm = StringManager.getManager(LifecycleBase.class);
    /**
     * 保存的事件监听器
     */
    private final List<LifecycleListener> lifecycleListeners = new CopyOnWriteArrayList<>();
    /**
     * 记录当前对象状态的实例
     * 默认为新建
     */
    private volatile LifecycleState state = LifecycleState.NEW;
    /**
     * 失败后是否抛出异常
     */
    private boolean throwOnFailure = true;


    /**
     * Will a {@link LifecycleException} thrown by a sub-class during
     * {@link #initInternal()}, {@link #startInternal()},
     * {@link #stopInternal()} or {@link #destroyInternal()} be re-thrown for
     * the caller to handle or will it be logged instead?
     *
     * @return {@code true} if the exception will be re-thrown, otherwise
     * {@code false}
     */
    public boolean getThrowOnFailure() {
        return throwOnFailure;
    }


    /**
     * Configure if a {@link LifecycleException} thrown by a sub-class during
     * {@link #initInternal()}, {@link #startInternal()},
     * {@link #stopInternal()} or {@link #destroyInternal()} will be re-thrown
     * for the caller to handle or if it will be logged instead.
     *
     * @param throwOnFailure {@code true} if the exception should be re-thrown,
     *                       otherwise {@code false}
     */
    public void setThrowOnFailure(boolean throwOnFailure) {
        this.throwOnFailure = throwOnFailure;
    }


    /**
     * 添加监听器
     */
    @Override
    public void addLifecycleListener(LifecycleListener listener) {
        lifecycleListeners.add(listener);
    }


    /**
     * 获取所有监听器
     */
    @Override
    public LifecycleListener[] findLifecycleListeners() {
        return lifecycleListeners.toArray(new LifecycleListener[0]);
    }


    /**
     * 移除一个监听器
     */
    @Override
    public void removeLifecycleListener(LifecycleListener listener) {
        lifecycleListeners.remove(listener);
    }


    /**
     * 实现触发事件并通知监听器对象，type为事件类型，data为事件携带的数据
     * 监听器触发相关的事件
     *
     * @param type Event type  事件类型
     * @param data Data associated with event. 传递给触发的监听器的数据
     */
    protected void fireLifecycleEvent(String type, Object data) {
        LifecycleEvent event = new LifecycleEvent(this, type, data);
        for (LifecycleListener listener : lifecycleListeners) {
            // 遍历获取所有的监听器-->触发
            listener.lifecycleEvent(event);
        }
    }


    /**
     * 实现了 Lifecycle 中定义的init方法
     * 该方法和对应的组件的状态产生的关联
     * 1.具体的组件初始化操作 Server  Service Connector Engine Host Context
     * 2.状态变化
     * 3.事件发布
     */
    @Override
    public final synchronized void init() throws LifecycleException {
        // 当前组件状态不是NEW状态则抛出无效状态异常
        if (!state.equals(LifecycleState.NEW)) {
            // 无效的操作  只有状态为 New 的才能调用init方法进入初始化
            invalidTransition(Lifecycle.BEFORE_INIT_EVENT);
        }

        try {
            // 设置状态为初始化进行中....同步在方法中会触发对应的事件
            setStateInternal(LifecycleState.INITIALIZING, null, false);
            // 交给子类具体的实现 初始化操作
            initInternal();
            // 更新状态为初始化完成 同步在方法中会触发对应的事件
            setStateInternal(LifecycleState.INITIALIZED, null, false);
        } catch (Throwable t) {
            handleSubClassException(t, "lifecycleBase.initFail", toString());
        }
    }


    /**
     * Sub-classes implement this method to perform any instance initialisation
     * required.
     *
     * @throws LifecycleException If the initialisation fails
     */
    protected abstract void initInternal() throws LifecycleException;


    /**
     * 和 init 方法相似的逻辑，其实就是简化了子类的处理
     * 在本方法中完成了 生命周期方法和相关事件及监听器的处理
     */
    @Override
    public final synchronized void start() throws LifecycleException {
        // 当前组件已经处于启动流程中或者已经启动完成时，打印输出
        if (LifecycleState.STARTING_PREP.equals(state) || LifecycleState.STARTING.equals(state) ||
            LifecycleState.STARTED.equals(state)) {

            if (log.isDebugEnabled()) {
                Exception e = new LifecycleException();
                log.debug(sm.getString("lifecycleBase.alreadyStarted", toString()), e);
            } else if (log.isInfoEnabled()) {
                log.info(sm.getString("lifecycleBase.alreadyStarted", toString()));
            }
            return;
        }
        // 状态为new，先执行初始化逻辑
        if (state.equals(LifecycleState.NEW)) {
            init();
        } else if (state.equals(LifecycleState.FAILED)) {
            // 状态为失败，先执行停止流程
            stop();
        } else if (!state.equals(LifecycleState.INITIALIZED) &&
            !state.equals(LifecycleState.STOPPED)) {
            // 状态不是初始化完成状态并且不是停止流程完成状态，抛出无效状态转换一场
            invalidTransition(Lifecycle.BEFORE_START_EVENT);
        }

        try {
            // 设置状态为启动之前
            setStateInternal(LifecycleState.STARTING_PREP, null, false);
            // 交给子类来具体的实现
            startInternal();
            // 子类将状态设置为failed时，进入停止流程
            if (state.equals(LifecycleState.FAILED)) {
                stop();
            } else if (!state.equals(LifecycleState.STARTING)) {
                // 如果子类没有开始失败，那么这里的状态应该是STARTING启动状态
                // 如果不是，抛出无效状态转换异常，因为只有STARTING才能转换为started
                invalidTransition(Lifecycle.AFTER_START_EVENT);
            } else {
                // 将状态转化为STARTED，表示所有依赖组件均已启动成功
                setStateInternal(LifecycleState.STARTED, null, false);
            }
        } catch (Throwable t) {
            // This is an 'uncontrolled' failure so put the component into the
            // FAILED state and throw an exception.
            handleSubClassException(t, "lifecycleBase.startFail", toString());
        }
    }


    /**
     * Sub-classes must ensure that the state is changed to
     * {@link LifecycleState#STARTING} during the execution of this method.
     * Changing state will trigger the {@link Lifecycle#START_EVENT} event.
     * <p>
     * If a component fails to start it may either throw a
     * {@link LifecycleException} which will cause it's parent to fail to start
     * or it can place itself in the error state in which case {@link #stop()}
     * will be called on the failed component but the parent component will
     * continue to start normally.
     *
     * @throws LifecycleException Start error occurred
     */
    protected abstract void startInternal() throws LifecycleException;


    /**
     * {@inheritDoc}
     */
    @Override
    public final synchronized void stop() throws LifecycleException {
        // 如果当前组件状态已经是停止中或者已经停止，那么打印异常并返回
        if (LifecycleState.STOPPING_PREP.equals(state) || LifecycleState.STOPPING.equals(state) ||
            LifecycleState.STOPPED.equals(state)) {
            if (log.isDebugEnabled()) {
                Exception e = new LifecycleException();
                log.debug(sm.getString("lifecycleBase.alreadyStopped", toString()), e);
            } else if (log.isInfoEnabled()) {
                log.info(sm.getString("lifecycleBase.alreadyStopped", toString()));
            }
            return;
        }
        // 当前状态为new状态，直接转变为stopped，因为此时内部组件都没有启动，所以不需要进入完整的停止周期
        if (state.equals(LifecycleState.NEW)) {
            state = LifecycleState.STOPPED;
            return;
        }
        // 只能从 started和failed 状态进入停止流程，否则抛出无效状态异常
        if (!state.equals(LifecycleState.STARTED) && !state.equals(LifecycleState.FAILED)) {
            invalidTransition(Lifecycle.BEFORE_STOP_EVENT);
        }

        try {
            // 如果当前组件状态是failed状态则触发before_stop_event事件
            if (state.equals(LifecycleState.FAILED)) {
                // Don't transition to STOPPING_PREP as that would briefly mark the
                // component as available but do ensure the BEFORE_STOP_EVENT is
                // fired
                fireLifecycleEvent(BEFORE_STOP_EVENT, null);
            } else {
                // 否则开始状态转变为STOPPING_PREP
                setStateInternal(LifecycleState.STOPPING_PREP, null, false);
            }
            // 子类停止方法，并将状态修改为STOPPING
            stopInternal();
            // 子类负责转变为stopping，或者进入failed状态，如果不是这两者抛出无效状态转换异常
            if (!state.equals(LifecycleState.STOPPING) && !state.equals(LifecycleState.FAILED)) {
                invalidTransition(Lifecycle.AFTER_STOP_EVENT);
            }
            // 将状态修改为stopped
            setStateInternal(LifecycleState.STOPPED, null, false);
        } catch (Throwable t) {
            handleSubClassException(t, "lifecycleBase.stopFail", toString());
        } finally {
            // 子类实现 SingleUse，表明对象只使用一次，那么在stop方法返回之后，将自动调用 destroy方法，进入销毁流程
            if (this instanceof Lifecycle.SingleUse) {
                // 将状态修改为stopped
                setStateInternal(LifecycleState.STOPPED, null, false);
                // 销毁流程
                destroy();
            }
        }
    }


    /**
     * Sub-classes must ensure that the state is changed to
     * {@link LifecycleState#STOPPING} during the execution of this method.
     * Changing state will trigger the {@link Lifecycle#STOP_EVENT} event.
     *
     * @throws LifecycleException Stop error occurred
     */
    protected abstract void stopInternal() throws LifecycleException;


    @Override
    public final synchronized void destroy() throws LifecycleException {
        // 当前状态为failed，先执行stop流程，因为此时可能有一些组件已经开始运行了，但是由于某些原因导致进入failed状态，所以有必要进入stop流程
        if (LifecycleState.FAILED.equals(state)) {
            try {
                stop();
            } catch (LifecycleException e) {
                // Just log. Still want to destroy.
                log.error(sm.getString("lifecycleBase.destroyStopFail", toString()), e);
            }
        }
        // 如果已经处于销毁或处于销毁流程中，那么打印异常并退出
        if (LifecycleState.DESTROYING.equals(state) || LifecycleState.DESTROYED.equals(state)) {
            if (log.isDebugEnabled()) {
                Exception e = new LifecycleException();
                log.debug(sm.getString("lifecycleBase.alreadyDestroyed", toString()), e);
            } else if (log.isInfoEnabled() && !(this instanceof Lifecycle.SingleUse)) {
                // Rather than have every component that might need to call
                // destroy() check for SingleUse, don't log an info message if
                // multiple calls are made to destroy()
                log.info(sm.getString("lifecycleBase.alreadyDestroyed", toString()));
            }

            return;
        }
        // 当前状态不是stopeed，failed，new，INITIALIZED抛出状态转换异常，因为只有这四种状态可以直接进入销毁流程
        if (!state.equals(LifecycleState.STOPPED) && !state.equals(LifecycleState.FAILED) &&
            !state.equals(LifecycleState.NEW) && !state.equals(LifecycleState.INITIALIZED)) {
            invalidTransition(Lifecycle.BEFORE_DESTROY_EVENT);
        }

        try {
            // 转变状态为DESTROYING
            setStateInternal(LifecycleState.DESTROYING, null, false);
            // 子类实现
            destroyInternal();
            // 转变状态为DESTROYED
            setStateInternal(LifecycleState.DESTROYED, null, false);
        } catch (Throwable t) {
            handleSubClassException(t, "lifecycleBase.destroyFail", toString());
        }
    }


    /**
     * Sub-classes implement this method to perform any instance destruction
     * required.
     *
     * @throws LifecycleException If the destruction fails
     */
    protected abstract void destroyInternal() throws LifecycleException;


    /**
     * 获取当前状态
     */
    @Override
    public LifecycleState getState() {
        return state;
    }


    /**
     * 获取当前状态名字
     */
    @Override
    public String getStateName() {
        return getState().toString();
    }


    /**
     * 设置当前组件的状态，不可以携带通知监听器的数据
     */
    protected synchronized void setState(LifecycleState state) throws LifecycleException {
        setStateInternal(state, null, true);
    }


    /**
     * 设置当前组件的状态，可以携带通知监听器的数据
     */
    protected synchronized void setState(LifecycleState state, Object data)
        throws LifecycleException {
        setStateInternal(state, data, true);
    }


    private synchronized void setStateInternal(LifecycleState state, Object data, boolean check)
        throws LifecycleException {
        // debug级别日志打印
        if (log.isDebugEnabled()) {
            log.debug(sm.getString("lifecycleBase.setState", this, state));
        }

        if (check) {
            // 不允许是null
            if (state == null) {
                invalidTransition("null");
                // Unreachable code - here to stop eclipse complaining about
                // a possible NPE further down the method
                return;
            }
            /*
             * 检查当前状态是否可以转变为指定状态，不能则抛出异常
             * 任何方法都可以转变为failed，因为任何方法的调用都有可能出错
             */
            if (!(state == LifecycleState.FAILED ||
                (this.state == LifecycleState.STARTING_PREP &&
                    state == LifecycleState.STARTING) ||
                (this.state == LifecycleState.STOPPING_PREP &&
                    state == LifecycleState.STOPPING) ||
                (this.state == LifecycleState.FAILED &&
                    state == LifecycleState.STOPPING))) {
                // 其他的状态转变方式不支持
                invalidTransition(state.name());
            }
        }
        // 修改当前组件对象的状态
        this.state = state;
        // 根据状态和事件的绑定关系获取对应的事件
        String lifecycleEvent = state.getLifecycleEvent();
        if (lifecycleEvent != null) {
            // 发布对应的事件  获取所有的监听器，执行触发的方法
            fireLifecycleEvent(lifecycleEvent, data);
        }
    }


    private void invalidTransition(String type) throws LifecycleException {
        String msg = sm.getString("lifecycleBase.invalidTransition", type, toString(), state);
        throw new LifecycleException(msg);
    }


    private void handleSubClassException(Throwable t, String key, Object... args) throws LifecycleException {
        setStateInternal(LifecycleState.FAILED, null, false);
        ExceptionUtils.handleThrowable(t);
        String msg = sm.getString(key, args);
        if (getThrowOnFailure()) {
            if (!(t instanceof LifecycleException)) {
                t = new LifecycleException(msg, t);
            }
            throw (LifecycleException) t;
        } else {
            log.error(msg, t);
        }
    }
}
