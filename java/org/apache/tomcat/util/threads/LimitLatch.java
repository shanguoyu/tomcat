/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.apache.tomcat.util.threads;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

/**
 * 在 Tomcat 中对于可接收的连接数是有限制的，在TCP3次握手后内核会将连接放入设置的backlog队列
 * 而Tomcat中对应的参数便是acceptCount，当这个队列满后，内核不再会接收客户端连接，这时会给客户端返回一个连接重置的信息
 * AbstractEndpoint类的属性maxConnections表明的便是Tomcat的接收连接的线程能够从backlog队列中取出连接，并将其封装为套接字处理对象处理的队列大小，backlog默认为100
 * maxConnections 默认为 10000，那么为什么需要设置这两者的大小呢?首先如果设置backlog 过大，那么将会导致资源浪费，因为 backlog 队列需要占用内核空间，其次，如果将 3 次握手
 * 的连接放入过大的 backlog 队列，队列末尾的客户端将得不到及时处理，客户端会有感知卡顿现象。同理maxConnections设置过大也会出现这样的情况，那么为什么需要两个队列呢?我们知道当使用accept
 * 方法获取连接时会进行上下文切换，这会消耗性能，那么何不如让接收线程在 Tomcat 来不及处理连接时，不断地先将内核中的连接从 backlog 队列中取出放入 Tomcat 内部的队列，当Tomcat 可以处理时就
 * 不用等待连接器上下文切换获取连接了。同时这个队列又不能过大，所以这里通过 LimitLatch 来控制maxConnections
 */
public class LimitLatch {
    /**
     * 日志国际化相关
     */
    private static final Log log = LogFactory.getLog(LimitLatch.class);
    /**
     * AQS
     */
    private final Sync sync;
    /**
     * 控制连接数的原子性计数器
     */
    private final AtomicLong count;
    /**
     * 用于限制最大连接数与 count 联合使用
     */
    private volatile long limit;
    /**
     * 标识是否释放成功
     */
    private volatile boolean released = false;
    /**
     */
    public LimitLatch(long limit) {
        this.limit = limit;
        this.count = new AtomicLong(0);
        this.sync = new Sync();
    }
    /**
     * AQS
     */
    private class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 1L;

        public Sync() {
        }

        @Override
        protected int tryAcquireShared(int ignored) {
            long newCount = count.incrementAndGet();
            if (!released && newCount > limit) {
                // Limit exceeded
                count.decrementAndGet();
                return -1;
            } else {
                return 1;
            }
        }

        @Override
        protected boolean tryReleaseShared(int arg) {
            count.decrementAndGet();
            return true;
        }
    }





    /**
     * Returns the current count for the latch
     * @return the current count for latch
     */
    public long getCount() {
        return count.get();
    }

    /**
     * Obtain the current limit.
     * @return the limit
     */
    public long getLimit() {
        return limit;
    }


    /**
     * Sets a new limit. If the limit is decreased there may be a period where
     * more shares of the latch are acquired than the limit. In this case no
     * more shares of the latch will be issued until sufficient shares have been
     * returned to reduce the number of acquired shares of the latch to below
     * the new limit. If the limit is increased, threads currently in the queue
     * may not be issued one of the newly available shares until the next
     * request is made for a latch.
     *
     * @param limit The new limit
     */
    public void setLimit(long limit) {
        this.limit = limit;
    }


    /**
     * Acquires a shared latch if one is available or waits for one if no shared
     * latch is current available.
     * @throws InterruptedException If the current thread is interrupted
     */
    public void countUpOrAwait() throws InterruptedException {
        if (log.isDebugEnabled()) {
            log.debug("Counting up["+Thread.currentThread().getName()+"] latch="+getCount());
        }
        sync.acquireSharedInterruptibly(1);
    }

    /**
     * Releases a shared latch, making it available for another thread to use.
     * @return the previous counter value
     */
    public long countDown() {
        sync.releaseShared(0);
        long result = getCount();
        if (log.isDebugEnabled()) {
            log.debug("Counting down["+Thread.currentThread().getName()+"] latch="+result);
        }
        return result;
    }

    /**
     * Releases all waiting threads and causes the {@link #limit} to be ignored
     * until {@link #reset()} is called.
     * @return <code>true</code> if release was done
     */
    public boolean releaseAll() {
        released = true;
        return sync.releaseShared(0);
    }

    /**
     * Resets the latch and initializes the shared acquisition counter to zero.
     * @see #releaseAll()
     */
    public void reset() {
        this.count.set(0);
        released = false;
    }

    /**
     * Returns <code>true</code> if there is at least one thread waiting to
     * acquire the shared lock, otherwise returns <code>false</code>.
     * @return <code>true</code> if threads are waiting
     */
    public boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

    /**
     * Provide access to the list of threads waiting to acquire this limited
     * shared latch.
     * @return a collection of threads
     */
    public Collection<Thread> getQueuedThreads() {
        return sync.getQueuedThreads();
    }
}
