/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.Container;
import org.apache.catalina.ContainerEvent;
import org.apache.catalina.ContainerListener;
import org.apache.catalina.Context;
import org.apache.catalina.Engine;
import org.apache.catalina.Host;
import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.LifecycleState;
import org.apache.catalina.Service;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.Wrapper;
import org.apache.catalina.util.LifecycleMBeanBase;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.apache.tomcat.util.res.StringManager;


/**
 * Mapper listener.
 *
 * @author Remy Maucherat
 * @author Costin Manolache
 */
public class MapperListener extends LifecycleMBeanBase
        implements ContainerListener, LifecycleListener {


    private static final Log log = LogFactory.getLog(MapperListener.class);


    // ----------------------------------------------------- Instance Variables
    /**
     * 关联的 Mapper 对象
     */
    private final Mapper mapper;

    /**
     * 关联的 Service 对象
     */
    private final Service service;
    /**
     * 国际化相关，省略
     */
    private static final StringManager sm =
        StringManager.getManager(Constants.Package);

    /**
     *
     */
    private final String domain = null;


    // ----------------------------------------------------------- Constructors

    /**
     * Create mapper listener.
     *
     * @param service The service this listener is associated with
     */
    public MapperListener(Service service) {
        this.service = service;
        this.mapper = service.getMapper();
    }


    // ------------------------------------------------------- Lifecycle Methods

    @Override
    public void startInternal() throws LifecycleException {
        // 设置状态为 STARTING
        setState(LifecycleState.STARTING);
        // 获得 Service 对应的 Engine
        Engine engine = service.getContainer();
        // 如果为 NULL 直接返回
        if (engine == null) {
            return;
        }
        // 获取并设置默认主机对象
        findDefaultHost();

        addListeners(engine);

        Container[] conHosts = engine.findChildren();
        for (Container conHost : conHosts) {
            Host host = (Host) conHost;
            if (!LifecycleState.NEW.equals(host.getState())) {
                // Registering the host will register the context and wrappers
                registerHost(host);
            }
        }
    }


    @Override
    public void stopInternal() throws LifecycleException {
        // 设置状态
        setState(LifecycleState.STOPPING);
        // 获取 Engine
        Engine engine = service.getContainer();
        if (engine == null) {
            return;
        }
        // 将当前监听器在容器中移除
        removeListeners(engine);
    }


    @Override
    protected String getDomainInternal() {
        if (service instanceof LifecycleMBeanBase) {
            return ((LifecycleMBeanBase) service).getDomain();
        } else {
            return null;
        }
    }


    @Override
    protected String getObjectNameKeyProperties() {
        // Same as connector but Mapper rather than Connector
        return ("type=Mapper");
    }

    // --------------------------------------------- Container Listener methods

    @Override
    public void containerEvent(ContainerEvent event) {
        // 添加子容器事件
        if (Container.ADD_CHILD_EVENT.equals(event.getType())) {
            // 获取携带的数据
            Container child = (Container) event.getData();
            // 添加监听器到子容器中
            addListeners(child);
            // 当前组件可用
            if (child.getState().isAvailable()) {
                // 如果添加的是 Host
                if (child instanceof Host) {
                    // 注册 Host
                    registerHost((Host) child);
                } else if (child instanceof Context) {
                    // 注册 Context
                    registerContext((Context) child);
                } else if (child instanceof Wrapper) {
                    if (child.getParent().getState().isAvailable()) {
                        // 注册 Wrapper
                        registerWrapper((Wrapper) child);
                    }
                }
            }
        } else if (Container.REMOVE_CHILD_EVENT.equals(event.getType())) {
            // 移除子容器事件
            Container child = (Container) event.getData();
            // 移除子容器监听器
            removeListeners(child);
        } else if (Host.ADD_ALIAS_EVENT.equals(event.getType())) {
            // 主机添加别名事件

            // 添加主机别名映射
            mapper.addHostAlias(((Host) event.getSource()).getName(),
                    event.getData().toString());
        } else if (Host.REMOVE_ALIAS_EVENT.equals(event.getType())) {
            // 移除主机别名事件
            mapper.removeHostAlias(event.getData().toString());
        } else if (Wrapper.ADD_MAPPING_EVENT.equals(event.getType())) {
            // Wrapper 添加映射事件
            Wrapper wrapper = (Wrapper) event.getSource();
            Context context = (Context) wrapper.getParent();
            String contextPath = context.getPath();
            if ("/".equals(contextPath)) {
                contextPath = "";
            }
            String version = context.getWebappVersion();
            String hostName = context.getParent().getName();
            String wrapperName = wrapper.getName();
            String mapping = (String) event.getData();
            boolean jspWildCard = ("jsp".equals(wrapperName)
                    && mapping.endsWith("/*"));
            mapper.addWrapper(hostName, contextPath, version, mapping, wrapper,
                    jspWildCard, context.isResourceOnlyServlet(wrapperName));
        } else if (Wrapper.REMOVE_MAPPING_EVENT.equals(event.getType())) {
            // Wrapper 移除映射事件
            Wrapper wrapper = (Wrapper) event.getSource();

            Context context = (Context) wrapper.getParent();
            String contextPath = context.getPath();
            if ("/".equals(contextPath)) {
                contextPath = "";
            }
            String version = context.getWebappVersion();
            String hostName = context.getParent().getName();

            String mapping = (String) event.getData();

            mapper.removeWrapper(hostName, contextPath, version, mapping);
        } else if (Context.ADD_WELCOME_FILE_EVENT.equals(event.getType())) {
            // Context 添加欢迎页事件
            Context context = (Context) event.getSource();

            String hostName = context.getParent().getName();

            String contextPath = context.getPath();
            if ("/".equals(contextPath)) {
                contextPath = "";
            }

            String welcomeFile = (String) event.getData();

            mapper.addWelcomeFile(hostName, contextPath,
                    context.getWebappVersion(), welcomeFile);
        } else if (Context.REMOVE_WELCOME_FILE_EVENT.equals(event.getType())) {
            // Context 移除欢迎页事件
            Context context = (Context) event.getSource();

            String hostName = context.getParent().getName();

            String contextPath = context.getPath();
            if ("/".equals(contextPath)) {
                contextPath = "";
            }

            String welcomeFile = (String) event.getData();

            mapper.removeWelcomeFile(hostName, contextPath,
                    context.getWebappVersion(), welcomeFile);
        } else if (Context.CLEAR_WELCOME_FILES_EVENT.equals(event.getType())) {
            // Context 清除欢迎页事件
            Context context = (Context) event.getSource();

            String hostName = context.getParent().getName();

            String contextPath = context.getPath();
            if ("/".equals(contextPath)) {
                contextPath = "";
            }

            mapper.clearWelcomeFiles(hostName, contextPath,
                    context.getWebappVersion());
        }
    }


    // ------------------------------------------------------ Protected Methods

    private void findDefaultHost() {
        // 获取 Engine
        Engine engine = service.getContainer();
        // 获取默认主机名
        String defaultHost = engine.getDefaultHost();
        boolean found = false;
        // 如果设置了默认主机，那么遍历引擎的子容器 Host，找到名字或者别名匹配的虚拟主机，然后设置 found 标志位
        if (defaultHost != null && defaultHost.length() > 0) {
            Container[] containers = engine.findChildren();

            for (Container container : containers) {
                Host host = (Host) container;
                if (defaultHost.equalsIgnoreCase(host.getName())) {
                    found = true;
                    break;
                }

                String[] aliases = host.findAliases();
                for (String alias : aliases) {
                    if (defaultHost.equalsIgnoreCase(alias)) {
                        found = true;
                        break;
                    }
                }
            }
        }
        // 如果找到了
        if (found) {
            // 将名字设置到 Mapper 中
            mapper.setDefaultHostName(defaultHost);
        } else {
            log.error(sm.getString("mapperListener.unknownDefaultHost", defaultHost, service));
        }
    }


    /**
     * Register host.
     */
    private void registerHost(Host host) {
        // 获得主机所有别名
        String[] aliases = host.findAliases();
        // 添加 host
        mapper.addHost(host.getName(), aliases, host);
        // 获得 host 下面所有子容器
        for (Container container : host.findChildren()) {
            // 如果当前容器状态是可用的
            if (container.getState().isAvailable()) {
                // 注册 Context
                registerContext((Context) container);
            }
        }

        findDefaultHost();

        if(log.isDebugEnabled()) {
            log.debug(sm.getString("mapperListener.registerHost",
                    host.getName(), domain, service));
        }
    }


    /**
     * Unregister host.
     */
    private void unregisterHost(Host host) {

        String hostname = host.getName();

        mapper.removeHost(hostname);

        // Default host may have changed
        findDefaultHost();

        if(log.isDebugEnabled()) {
            log.debug(sm.getString("mapperListener.unregisterHost", hostname,
                    domain, service));
        }
    }


    /**
     * Unregister wrapper.
     */
    private void unregisterWrapper(Wrapper wrapper) {

        Context context = ((Context) wrapper.getParent());
        String contextPath = context.getPath();
        String wrapperName = wrapper.getName();

        if ("/".equals(contextPath)) {
            contextPath = "";
        }
        String version = context.getWebappVersion();
        String hostName = context.getParent().getName();

        String[] mappings = wrapper.findMappings();

        for (String mapping : mappings) {
            mapper.removeWrapper(hostName, contextPath, version,  mapping);
        }

        if(log.isDebugEnabled()) {
            log.debug(sm.getString("mapperListener.unregisterWrapper",
                    wrapperName, contextPath, service));
        }
    }


    /**
     * Register context.
     */
    private void registerContext(Context context) {
        // 获得 Context 的 path
        String contextPath = context.getPath();
        // 如果是 / 则重置为 ""
        if ("/".equals(contextPath)) {
            contextPath = "";
        }
        // 获取 parent 也就是 host
        Host host = (Host)context.getParent();
        // 获取上下文资源，该对象当作 Web 应用程序的所有资源，包括 jar 包，class 等
        WebResourceRoot resources = context.getResources();
        // 后去欢迎页文件
        String[] welcomeFiles = context.findWelcomeFiles();
        // Wrappers 映射信息列表
        List<WrapperMappingInfo> wrappers = new ArrayList<>();
        // 遍历所有子容器
        for (Container container : context.findChildren()) {
            // 用上下文中的 Wrapper 映射注册信息，添加到 Wrappers 列表中
            prepareWrapperMappingInfo(context, (Wrapper) container, wrappers);

            if(log.isDebugEnabled()) {
                log.debug(sm.getString("mapperListener.registerWrapper",
                        container.getName(), contextPath, service));
            }
        }
        // 将当前上下文的所有信息全部添加到 mapper 中
        mapper.addContextVersion(host.getName(), host, contextPath,
                context.getWebappVersion(), context, welcomeFiles, resources,
                wrappers);

        if(log.isDebugEnabled()) {
            log.debug(sm.getString("mapperListener.registerContext",
                    contextPath, service));
        }
    }


    /**
     * Unregister context.
     */
    private void unregisterContext(Context context) {

        String contextPath = context.getPath();
        if ("/".equals(contextPath)) {
            contextPath = "";
        }
        String hostName = context.getParent().getName();

        if (context.getPaused()) {
            if (log.isDebugEnabled()) {
                log.debug(sm.getString("mapperListener.pauseContext",
                        contextPath, service));
            }

            mapper.pauseContextVersion(context, hostName, contextPath,
                    context.getWebappVersion());
        } else {
            if (log.isDebugEnabled()) {
                log.debug(sm.getString("mapperListener.unregisterContext",
                        contextPath, service));
            }

            mapper.removeContextVersion(context, hostName, contextPath,
                    context.getWebappVersion());
        }
    }


    /**
     * Register wrapper.
     */
    private void registerWrapper(Wrapper wrapper) {
        // 获得 Context
        Context context = (Context) wrapper.getParent();
        // 获得 path，如果 path 是/ 则重置为 ""
        String contextPath = context.getPath();
        if ("/".equals(contextPath)) {
            contextPath = "";
        }
        // 获得 version
        String version = context.getWebappVersion();
        // 获得 hostName
        String hostName = context.getParent().getName();
        List<WrapperMappingInfo> wrappers = new ArrayList<>();
        // 添加映射
        prepareWrapperMappingInfo(context, wrapper, wrappers);
        // 添加 wrappers 到 mapper 中
        mapper.addWrappers(hostName, contextPath, version, wrappers);

        if(log.isDebugEnabled()) {
            log.debug(sm.getString("mapperListener.registerWrapper",
                    wrapper.getName(), contextPath, service));
        }
    }

    /**
     * Populate <code>wrappers</code> list with information for registration of
     * mappings for this wrapper in this context.
     *
     * @param context
     * @param wrapper
     * @param wrappers
     */
    private void prepareWrapperMappingInfo(Context context, Wrapper wrapper,
            List<WrapperMappingInfo> wrappers) {
        // 获得 Wrapper 名称
        String wrapperName = wrapper.getName();
        // 获取资源是否只读
        boolean resourceOnly = context.isResourceOnlyServlet(wrapperName);
        // 获得所有映射
        String[] mappings = wrapper.findMappings();
        // 遍历所有映射
        for (String mapping : mappings) {
            // 如果 wrapperName 是 jsp 并且 匹配所有路径
            boolean jspWildCard = (wrapperName.equals("jsp")
                                   && mapping.endsWith("/*"));
            // 添加到 wrappers 中
            wrappers.add(new WrapperMappingInfo(mapping, wrapper, jspWildCard,
                    resourceOnly));
        }
    }

    @Override
    public void lifecycleEvent(LifecycleEvent event) {
        // 组件开始后方法返回前发出的事件字符串对象
        if (event.getType().equals(Lifecycle.AFTER_START_EVENT)) {
            // 获取事件源
            Object obj = event.getSource();
            // 如果是 Wrapper
            if (obj instanceof Wrapper) {
                // 将 Wrapper 注册到 Mapper中
                Wrapper w = (Wrapper) obj;
                if (w.getParent().getState().isAvailable()) {
                    registerWrapper(w);
                }
            } else if (obj instanceof Context) {
                // 如果是 Context，则将 Context 注册到 Mapper 中
                Context c = (Context) obj;
                if (c.getParent().getState().isAvailable()) {
                    registerContext(c);
                }
            } else if (obj instanceof Host) {
                // Host 也一样
                registerHost((Host) obj);
            }
        } else if (event.getType().equals(Lifecycle.BEFORE_STOP_EVENT)) {
            // 组件停止前发出的事件字符串对象
            Object obj = event.getSource();
            if (obj instanceof Wrapper) {
                // 从 Mapper 中移除
                unregisterWrapper((Wrapper) obj);
            } else if (obj instanceof Context) {
                // 从 Mapper 中移除
                unregisterContext((Context) obj);
            } else if (obj instanceof Host) {
                // 从 Mapper 中移除
                unregisterHost((Host) obj);
            }
        }
    }


    /**
     * Add this mapper to the container and all child containers
     *
     * @param container
     */
    private void addListeners(Container container) {
        // 添加当前监听器到容器中
        container.addContainerListener(this);
        // 添加当前监听器到容器中
        container.addLifecycleListener(this);
        // 获取容器所有子组件然后添加进去，递归
        for (Container child : container.findChildren()) {
            addListeners(child);
        }
    }


    /**
     * Remove this mapper from the container and all child containers
     *
     * @param container
     */
    private void removeListeners(Container container) {
        // 将当前监听器在容器中移除
        container.removeContainerListener(this);
        container.removeLifecycleListener(this);
        for (Container child : container.findChildren()) {
            removeListeners(child);
        }
    }
}
