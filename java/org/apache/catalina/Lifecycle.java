/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.catalina;


/**
 * Common interface for component life cycle methods.  Catalina components
 * may implement this interface (as well as the appropriate interface(s) for
 * the functionality they support) in order to provide a consistent mechanism
 * to start and stop the component.
 * Lifecycle接口定义了Tomcat中所有组件的生命周期函数，定义该接口，
 * 主要是为了统一管理组件的生命周期。该接口定义了常用事件字符串和监听器的支持函数
 * <br>
 * The valid state transitions for components that support {@link Lifecycle}
 * are:
 * 实现了Lifecycle接口的组件都支持如下的状态转换
 * <pre>
 *            start()
 *  -----------------------------
 *  |                           |
 *  | init()                    |
 * NEW -»-- INITIALIZING        |
 * | |           |              |     ------------------«-----------------------
 * | |           |auto          |     |                                        |
 * | |          \|/    start() \|/   \|/     auto          auto         stop() |
 * | |      INITIALIZED --»-- STARTING_PREP --»- STARTING --»- STARTED --»---  |
 * | |         |                                                            |  |
 * | |destroy()|                                                            |  |
 * | --»-----«--    ------------------------«--------------------------------  ^
 * |     |          |                                                          |
 * |     |         \|/          auto                 auto              start() |
 * |     |     STOPPING_PREP ----»---- STOPPING ------»----- STOPPED -----»-----
 * |    \|/                               ^                     |  ^
 * |     |               stop()           |                     |  |
 * |     |       --------------------------                     |  |
 * |     |       |                                              |  |
 * |     |       |    destroy()                       destroy() |  |
 * |     |    FAILED ----»------ DESTROYING ---«-----------------  |
 * |     |                        ^     |                          |
 * |     |     destroy()          |     |auto                      |
 * |     --------»-----------------    \|/                         |
 * |                                 DESTROYED                     |
 * |                                                               |
 * |                            stop()                             |
 * ----»-----------------------------»------------------------------
 *
 * Any state can transition to FAILED.
 * 任何状态都可以转换为FAILED状态。
 * Calling start() while a component is in states STARTING_PREP, STARTING or
 * STARTED has no effect.  当组件处于STARTING_PREP、STARTING或STARTED状态时调用start()是无效的
 *
 * Calling start() while a component is in state NEW will cause init() to be
 * called immediately after the start() method is entered.
 *
 * Calling stop() while a component is in states STOPPING_PREP, STOPPING or
 * STOPPED has no effect.
 *
 * Calling stop() while a component is in state NEW transitions the component
 * to STOPPED. This is typically encountered when a component fails to start and
 * does not start all its sub-components. When the component is stopped, it will
 * try to stop all sub-components - even those it didn't start.
 *
 * Attempting any other transition will throw {@link LifecycleException}.
 *
 * </pre>
 * The {@link LifecycleEvent}s fired during state changes are defined in the
 * methods that trigger the changed. No {@link LifecycleEvent}s are fired if the
 * attempted transition is not valid.
 *
 * @author Craig R. McClanahan
 */
public interface Lifecycle {
    /**
     * 组件初始化之前发出的事件字符串对象
     */
    public static final String BEFORE_INIT_EVENT = "before_init";
    /**
     * 组件初始化之后发出的事件字符串对象
     */
    public static final String AFTER_INIT_EVENT = "after_init";
    /**
     * 组件开始后发出的事件字符串对象
     */
    public static final String START_EVENT = "start";
    /**
     * 组件开始前发出的事件字符串对象
     */
    public static final String BEFORE_START_EVENT = "before_start";
    /**
     * 组件开始后方法返回前发出的事件字符串对象
     */
    public static final String AFTER_START_EVENT = "after_start";
    /**
     * 组件停止后发出的事件字符串对象
     */
    public static final String STOP_EVENT = "stop";
    /**
     * 组件停止前发出的事件字符串对象
     */
    public static final String BEFORE_STOP_EVENT = "before_stop";
    /**
     * 组件停止后方法返回前的事件字符串对象
     */
    public static final String AFTER_STOP_EVENT = "after_stop";
    /**
     * 组件销毁后方法返回前发出的事件字符串对象
     */
    public static final String AFTER_DESTROY_EVENT = "after_destroy";
    /**
     * 组件销毁前方法返回前发出的事件字符串对象
     */
    public static final String BEFORE_DESTROY_EVENT = "before_destroy";
    /**
     * 周期性事件（后台线程定时执行一些事情，比如：热部署、热替换）
     */
    public static final String PERIODIC_EVENT = "periodic";
    /**
     * 事件类型为组件开始配置事件字符串对象
     */
    public static final String CONFIGURE_START_EVENT = "configure_start";
    /**
     * 事件类型为组件停止配置事件字符串对象
     */
    public static final String CONFIGURE_STOP_EVENT = "configure_stop";


    /**
     * 向组件中添加监听器
     *
     * @param listener
     */
    public void addLifecycleListener(LifecycleListener listener);

    /**
     * 获取组件中所有监听器
     *
     * @return
     */
    public LifecycleListener[] findLifecycleListeners();

    /**
     * 移除一个监听器
     */
    public void removeLifecycleListener(LifecycleListener listener);

    /**
     * 组件初始化方法
     * 应该在该方法中初始化所有需要的对象。当所有需要的对象初始化后，将会调用监听器传递INIT_EVENT事件
     */
    public void init() throws LifecycleException;

    /**
     * 组件开始方法。当方法调用后，表明组件已经开始运行。在该方法中最开始调用时，监听器发出before_start_event事件
     * 这时组件的生命周期状态转变为starting_prep，当所有组件依赖的子组件完成了启动后，将发出start_event事件，这时状态转变为 starting，当方法返回前，将发出after_start_event事件
     * 将状态转变为started
     */
    public void start() throws LifecycleException;

    /**
     * 组件停止方法。当方法调用后，内部所有依赖的组件也应该都关闭。在该方法开始调用时，将发出before_stop_event事件，随后状态转变为stopping_prep
     * 当所有组件都关闭后，将发出stop_event事件，随后状态转变为stopping，在方法返回前将发出after_stop_event事件，随后状态变为stopped
     */
    public void stop() throws LifecycleException;

    /**
     * 组件销毁方法。当方法调用后，内部所有依赖的组件也应该都被销毁。方法调用后将发出destroy_event事件
     */
    public void destroy() throws LifecycleException;

    /**
     * 获取当前组建的状态
     */
    public LifecycleState getState();

    /**
     * 获取当前组件状态对应的字符串名
     */
    public String getStateName();


    /**
     * 标记接口，标记类的对象只能被使用一次
     */
    public interface SingleUse {
    }
}
