/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.apache.coyote;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.apache.tomcat.util.net.AbstractEndpoint.Handler.SocketState;
import org.apache.tomcat.util.net.SSLSupport;
import org.apache.tomcat.util.net.SocketEvent;
import org.apache.tomcat.util.net.SocketWrapperBase;

/**
 * Common interface for processors of all protocols.
 */
public interface Processor {

    /**
     * 处理客户端连接
     * @param socketWrapper 为 Socket 包装器包含了所有客户端的操作，SocketEvent 表示当前执行时的 socket 事件，读，写，停止，超时等状态
     * @param status
     * @return
     * @throws IOException
     */
    SocketState process(SocketWrapperBase<?> socketWrapper, SocketEvent status) throws IOException;

    /**
     * 用于升级协议包装信息，例如通过 HTTP1.1 升级为 HTTP2.0，当然这里只关注 HTTP1.1
     * @return
     */
    UpgradeToken getUpgradeToken();

    /**
     * 标识当前是否处理升级协议
     */
    boolean isUpgrade();

    /**
     * 标识当前是否为异步处理
     */
    boolean isAsync();

    /**
     * 用于检测当前处理器是否已经处理客户端超时
     */
    void timeoutAsync(long now);

    /**
     * 获取与当前处理器关联的客户端请求对象
     */
    Request getRequest();

    /**
     * 回收当前处理器对象
     */
    void recycle();

    /**
     * 设置是否支持 SSL 安全套接字
     */
    void setSslSupport(SSLSupport sslSupport);

    /**
     * 获取在协议升级过程中保存的额外数据
     */
    ByteBuffer getLeftoverInput();

    /**
     * 暂停当前协议处理器
     */
    void pause();

    /**
     * 检测异步超时
     */
    boolean checkAsyncTimeoutGeneration();
}
